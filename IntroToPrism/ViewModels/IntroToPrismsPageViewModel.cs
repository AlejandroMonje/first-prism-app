﻿using Prism.Mvvm;
using System.Diagnostics;

namespace IntroToPrism.ViewModels
{
    public class IntroToPrismsPageViewModel : BindableBase
    {
        public IntroToPrismsPageViewModel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}: ctor");

        }
    }
}